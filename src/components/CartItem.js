import React from 'react';
import thumb from '../images/thumb.png';

const CartItem = ({ item, onChangeHandler, showNetPrice }) => {
  return (
    <div className='row' key={item.id}>
      <div className='col-md-2'>
        <img src={thumb} className='img-fluid' alt='Thumbnail' />
      </div>
      <div className='col-md-2'>{item.name}</div>
      <div className='col-md-2'>
        <input
          type='number'
          min='1'
          max='999'
          value={item.quantity}
          onChange={(e) => onChangeHandler(e, item.id)}
        />
      </div>
      <div className='col-md-2 txtright'>{`$ ${item.price}`}</div>
      <div className='col-md-2 txtright'>{`$ ${item.grossPrice}`}</div>
      <div className='col-md-2 txtright'>
        {showNetPrice && `$ ${item.netPrice}`}
      </div>
    </div>
  );
};

export default CartItem;
