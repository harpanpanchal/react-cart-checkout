import React from 'react';
const CartHeader = () => {
  return (
    <div className='row'>
      <div className='col-md-2'>
        <strong>Image</strong>
      </div>
      <div className='col-md-2'>
        <strong>Name</strong>
      </div>
      <div className='col-md-2'>
        <strong>Quantity</strong>
      </div>
      <div className='col-md-2 txtright'>
        <strong>Price</strong>
      </div>
      <div className='col-md-2 txtright'>
        <strong>Gross Price</strong>
      </div>
      <div className='col-md-2 txtright'>
        <strong>Net Price</strong>
      </div>
    </div>
  );
};

export default CartHeader;
