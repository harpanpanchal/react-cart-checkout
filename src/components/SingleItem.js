import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import thumb from '../images/thumb.png';

const SingleItem = ({ item }) => {
  const { addToCart } = useContext(GlobalContext);

  return (
    <div className='col-md-4 mb-4'>
      <div className='card mb-4 shadow-sm h-100'>
        <img src={thumb} className='img-fluid' alt='Thumbnail' />
        <div className='card-body'>
          <h3 className='text-center'>{item.name}</h3>
          <h4 className='text-center'>{`$ ${item.price}`}</h4>
          <div className='d-flex justify-content-center align-items-center'>
            <button
              onClick={() => addToCart(item)}
              className='btn btn-sm btn-outline-secondary'
            >
              Add To Cart
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleItem;
