import React, { useState, useEffect, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import CartHeader from './CartHeader';
import CartItem from './CartItem';
import { validatePromoMessage } from '../utility/Utility';

const CheckOut = () => {
  const [promo, setPromo] = useState('');
  const [promoError, setPromoError] = useState(false);
  const {
    updateQuantity,
    data,
    updateNetPrice,
    updateNetasGrossPrice,
  } = useContext(GlobalContext);
  const [showNetPrice, setShowNetPrice] = useState(false);
  const [showFinalPrice, setShowFinalPrice] = useState(false);
  const [finalPrice, setFinalPrice] = useState('');

  useEffect(() => {
    calculateFinalPrice();
  }, [data]);

  const togglePrices = (val) => {
    setShowNetPrice(val);
    setShowFinalPrice(val);
  };

  const calculateFinalPrice = () => {
    const sum =
      data.cartItems.length > 0 &&
      data.cartItems.reduce((total, currentValue) => {
        return parseFloat(total) + parseFloat(currentValue.netPrice);
      }, 0);

    setFinalPrice(parseFloat(sum).toFixed(2));
  };

  const onChangeHandler = (e, id) => {
    togglePrices(false);
    updateQuantity(e.target.value, id);
  };

  const calculatePriceHandler = () => {
    updateNetasGrossPrice();
    if (promo === '') {
      updateNetasGrossPrice();
      calculateFinalPrice();
      togglePrices(true);
    } else {
      const checkDetails = data.promo.filter((item) => item.code === promo);

      if (checkDetails !== undefined && checkDetails.length === 1) {
        data.cartItems.length > 0 &&
          data.cartItems.map((cItem) => {
            if (
              checkDetails[0].discountType === 'totalamount' &&
              checkDetails[0].minimumamount < cItem.grossPrice
            ) {
              const netPrice =
                cItem.grossPrice -
                (cItem.grossPrice * checkDetails[0].discountPercentage) / 100;
              updateNetPrice(cItem.id, netPrice.toFixed(2));
            } else if (
              checkDetails[0].discountType === 'totalamount' &&
              checkDetails[0].minimumamount > cItem.grossPrice
            ) {
              updateNetPrice(cItem.id, cItem.grossPrice);
            } else if (
              checkDetails[0].discountType === 'totalitems' &&
              checkDetails[0].SpecialItem === true
            ) {
              const productCheck =
                data.cartItems.length > 0 &&
                data.cartItems.find(
                  (item) =>
                    item.id === checkDetails[0].minimumItemsProdCode &&
                    item.quantity >= checkDetails[0].minimumItems
                );
              if (productCheck !== undefined) {
                const updatedNetPrice =
                  parseFloat(checkDetails[0].fixedPricePerItem) *
                  cItem.quantity;
                updateNetPrice(
                  checkDetails[0].productsApplicable,
                  updatedNetPrice.toFixed(2)
                );
              }
            }
            return false;
          });
        calculateFinalPrice();
        togglePrices(true);
      }
    }
  };

  const onPromocodeHandler = (e) => {
    setPromo(e.target.value);
    const promovalid = data.promo.find((item) => item.code === e.target.value);
    promovalid ? setPromoError(true) : setPromoError(false);
  };

  return (
    <div
      id='gridSystemModal'
      className='modal fade'
      tabIndex='-1'
      role='dialog'
      aria-labelledby='gridModalLabel'
      aria-hidden='true'
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title' id='gridModalLabel'>
              Checkout
            </h5>
            <button
              type='button'
              className='close'
              data-dismiss='modal'
              aria-label='Close'
            >
              <span aria-hidden='true'>×</span>
            </button>
          </div>
          <div className='modal-body'>
            <div className='container'>
              <CartHeader />
              {data.cartItems &&
                data.cartItems.length > 0 &&
                data.cartItems.map((item) => (
                  <CartItem
                    key={item.id}
                    item={item}
                    onChangeHandler={onChangeHandler}
                    showNetPrice={showNetPrice}
                  />
                ))}
              <div className='row'>
                <div className='col-md-9 txtright'>
                  <strong>Add Promocode:</strong>
                </div>
                <div className='col-md-3 txtright'>
                  <input
                    type='text'
                    placeholder='Promocode'
                    value={promo}
                    className='promoInput'
                    onChange={onPromocodeHandler}
                  />

                  <p className='promovalidation'>
                    {validatePromoMessage(promo, promoError)}
                  </p>
                </div>
              </div>

              <div className='row'>
                <div className='col-md-9 txtright'>
                  <strong>Total Price:</strong>
                </div>
                <div className='col-md-3 txtright'>
                  {showFinalPrice && `$ ${finalPrice}`}
                </div>
              </div>
            </div>
          </div>
          <div className='modal-footer'>
            <button
              type='button'
              className='btn btn-primary'
              onClick={calculatePriceHandler}
            >
              Check Total Price
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CheckOut;
