import React, { useState, useEffect, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';

import SingleItem from './SingleItem';

const ListItem = () => {
  const [items, setItems] = useState([]);
  const { data } = useContext(GlobalContext);
  useEffect(() => {
    setItems(data.products);
  }, [data.products]);

  const renderContent = () => {
    return (
      <div className='container'>
        <div className='row'>
          {items.length > 0
            ? items.map((item) => <SingleItem key={item.id} item={item} />)
            : 'No Records Found'}
        </div>
      </div>
    );
  };

  return renderContent();
};

export default ListItem;
