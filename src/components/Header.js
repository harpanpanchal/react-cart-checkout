import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';

const Header = () => {
  const { data } = useContext(GlobalContext);
  return (
    <>
      <nav className='navbar navbar-dark bg-dark justify-content-between mb-5'>
        <a className='navbar-brand' href='/'>
          Logo
        </a>
        {data && data.cartItems && data.cartItems.length > 0 && (
          <button
            className='btn btn-outline-success my-2 my-sm-0'
            data-toggle='modal'
            data-target='#gridSystemModal'
          >
            Checkout
          </button>
        )}
      </nav>
      <div className='container text-center mb-5'>
        <h1>Latest Products</h1>
        <p>List of all the latest products with huge discount.</p>
      </div>
    </>
  );
};

export default Header;
