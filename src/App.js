import React from 'react';
import { GlobalProvider } from './context/GlobalState';
import ListItem from './components/ListItem';
import Header from './components/Header';
import CheckOut from './components/CheckOut';
import './styles/styles.scss';

function App() {
  return (<>
        <GlobalProvider>
    <Header />
    <ListItem />
      <CheckOut />
      </GlobalProvider>
  </>
  );
}

export default App;