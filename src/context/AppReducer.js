export default (state, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      const setDefaultPrice = parseFloat(action.payload.price * 1).toFixed(2);

      let addDetails;

      if (state.cartItems.length >= 0) {
        const dataCheck = state.cartItems.find(
          (item) => item.id === action.payload.id
        );

        if (!dataCheck) {
          addDetails = {
            ...action.payload,
            quantity: 1,
            grossPrice: setDefaultPrice,
            netPrice: setDefaultPrice,
          };
          return {
            ...state,
            cartItems: [...state.cartItems, addDetails],
          };
        }
      }

      return {
        ...state,
      };

    case 'UPDATE_QUANTITY':
      const { quantity, id } = action.payload;

      const updateData = state.cartItems.map((item) => {
        if (item.id === id) {
          return {
            ...item,
            quantity: Number(quantity),
            grossPrice: parseFloat(item.price * Number(quantity)).toFixed(2),
            netPrice: parseFloat(item.price * Number(quantity)).toFixed(2),
          };
        }
        return item;
      });

      return {
        ...state,
        cartItems: [...updateData],
      };

    case 'UPDATE_NETPRICE_AS_GROSSPRICE':
      const updateNetAsGross = state.cartItems.map((item) => {
        return {
          ...item,
          netPrice: item.grossPrice,
        };
      });

      return {
        ...state,
        cartItems: [...updateNetAsGross],
      };

    case 'UPDATE_NETPRICE':
      const { cartId, netPrice } = action.payload;

      const updateNetPrice = state.cartItems.map((item) => {
        if (item.id === cartId) {
          return {
            ...item,
            netPrice,
          };
        }
        return item;
      });

      return {
        ...state,
        cartItems: [...updateNetPrice],
      };

    default:
      return state;
  }
};
