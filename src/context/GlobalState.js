import React, { createContext, useReducer } from 'react';
import AppReducer from './AppReducer';

// Initial State

const initialState = {
  products: [
    {
      id: 'wf',
      name: 'Workflow',
      price: 199.99,
      promoApplicable: 'All',
    },
    {
      id: 'docgen',
      name: 'Document Generation',
      price: 9.99,
    },
    {
      id: 'form',
      name: 'Form',
      price: 99.99,
    },
  ],
  promo: [
    {
      id: 1,
      code: 'RRD4D32',
      description: '10% discount for orders above $1000 (pre-discount)',
      discountType: 'totalamount',
      SpecialItem: false,
      minimumamount: 1000,
      productsApplicable: 'All',
      combinationPurchase: false,
      discountPercentage: 10,
      minimumItems: 0,
      minimumItemsProdCode: '',
      fixedPricePerItem: null,
    },
    {
      id: 2,
      code: '44F4T11',
      description: '15% discount for orders above $1500 (pre-discount)',
      discountType: 'totalamount',
      SpecialItem: false,
      minimumamount: 1500,
      productsApplicable: 'All',
      combinationPurchase: false,
      discountPercentage: 15,
      minimumItems: 0,
      minimumItemsProdCode: '',
      fixedPricePerItem: null,
    },
    {
      id: 3,
      code: 'FF9543D1',
      description:
        'Reduces the docgen price to $8.99 a unit when at least 10 documents are purchased',
      discountType: 'totalitems',
      SpecialItem: true,
      minimumamount: null,
      productsApplicable: 'docgen',
      combinationPurchase: true,
      discountPercentage: null,
      minimumItems: 10,
      minimumItemsProdCode: 'docgen',
      fixedPricePerItem: 8.99,
    },
    {
      id: 4,
      code: 'YYGWKJD',
      description:
        'Reduces the form price to $89.99 a unit when at least 1 wf is purchased',
      discountType: 'totalitems',
      SpecialItem: true,
      minimumamount: null,
      productsApplicable: 'form',
      combinationPurchase: true,
      discountPercentage: null,
      minimumItems: 1,
      minimumItemsProdCode: 'wf',
      fixedPricePerItem: 89.99,
    },
  ],
  cartItems: [],
};

// Create Context

export const GlobalContext = createContext(initialState);

// Provider Component

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  // Actions

  const addToCart = (product) => {
    dispatch({
      type: 'ADD_TO_CART',
      payload: product,
    });
  };

  const updateQuantity = (value, id) => {
    const data = {
      quantity: value,
      id,
    };
    dispatch({
      type: 'UPDATE_QUANTITY',
      payload: data,
    });
  };

  const updateNetasGrossPrice = () => {
    dispatch({
      type: 'UPDATE_NETPRICE_AS_GROSSPRICE',
    });
  };

  const updateNetPrice = (cartId, netPrice) => {
    const data = {
      cartId,
      netPrice,
    };
    dispatch({
      type: 'UPDATE_NETPRICE',
      payload: data,
    });
  };

  return (
    <GlobalContext.Provider
      value={{
        data: state,
        addToCart,
        updateQuantity,
        updateNetPrice,
        updateNetasGrossPrice,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
