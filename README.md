## Project Specifications

### Installation

1. Run 'npm install' to install all the packages and dependencies.

### Run the project

1. Run 'npm start' to run the project in localhost.

### Folder Structure

1. Project has been built using React / React Hooks.
2. Data has been stored in the 'context/GlobalState.js' using React Context API.
3. AppReducer.js provides new state based on the data passed from actions / functions.
4. All the functional components have been kept inside 'Component' folder.
5. Style files have been stored inside 'Styles' folder.
6. Utility functions can be found inside utility folder.
7. Bootstrap CSS framework has been implemented as well.

### Important points

1. List items / Single Item components have been used to render the products on the main page. As soon as we click 'Add to Cart' button, it displays the 'Checkout' button on the top right section of the main page. Popup dialog box will be opened if user clicks 'Checkout' button and based on the promocode, it would update the net price.
